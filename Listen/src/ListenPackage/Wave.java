package ListenPackage;
import java.awt.geom.Point2D;
import robocode.*;


public class Wave
{
	//-------comparable varialbles---------
	public double linePreDiffAngle;
	public double distance;
	public double distanceToCenter;
	//----------------------------------
	Point2D.Double startPoint;
	public long   startTime;
	public double absBearing;
	public double velocity;   //最后这是一个距离的值
	public double power;
	
	public double willHitDiffAngle;
	public Wave(Point2D.Double startPoint,double power,long time,double bearing,double dist,double lpda,double distanceToCenter){
		velocity=20-power*3;
		if(power==0){
			velocity=11;
		}
		this.power=power;
		absBearing=bearing;
		distance=dist/90d;
        linePreDiffAngle=lpda;
		this.distanceToCenter=distanceToCenter/75d;
		this.startPoint=startPoint;
        startTime=time;

		willHitDiffAngle=10D;
	}
	public static double getComVal(Object o,Object o2,double dir){
		Wave curWave=(Wave)o;
		Wave comWave=(Wave)o2;
        return Point2D.distanceSq(curWave.linePreDiffAngle,curWave.distance,comWave.linePreDiffAngle*dir,comWave.distance)
			   +Math.pow((curWave.distanceToCenter-comWave.distanceToCenter),2);
	}
	/**
	 * test the wave (when the wave hit enemy,test succede)
	 */
	public void test(Point2D.Double enemyPoint,long time){
		if(willHitDiffAngle==10D){						//测试打出这个WAVE是否符合要求
		    double dist=enemyPoint.distance(startPoint);
			double travel=velocity*(time-startTime);
			if (travel<0){
				willHitDiffAngle=20D;    //没有打中就转大一点
			}else if(Math.abs(dist-travel)<18){  
				willHitDiffAngle=robocode.util.Utils.normalRelativeAngle(getAngle(enemyPoint,startPoint)-absBearing);//如果击中目标就获取敌人的相关数据
                velocity=dist; //记录能打到的这个距离
			}
		}
	}
	public static double getAngle(Point2D.Double p2,Point2D.Double p1){
		return Math.atan2(p2.x-p1.x,p2.y-p1.y);
	}
};
