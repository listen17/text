package ListenPackage;
import robocode.*;
import java.awt.Color;
import java.util.Vector;
import java.awt.geom.Point2D;
import ListenPackage.Wave;
/**
 * 第一次接触到ROBOCODE 的东西，虽然比较老但是还是觉得还是学到了些东西
 *------------------------------------------------------------------------------- ---------------------------------Learn from Cigaret------------------------------------------
**/ 
public class Listen extends AdvancedRobot
{
	private static Vector waves=new Vector();     //enemy's patterns
    private static double preDiffAngle;           //gun 需要转的提前角度
	private static double power=0;                //fire 射出的能量
	private static double enemyEnergy;            //enemy's 所拥有的energy
	private static double lastEnemyBulletPower;   
	private static long   enemyLastFireTime;      

	public void run( ) {								//做一些坦克的初始化设置
		setColors(Color.white,Color.white,new Color(140,15,16));
		setAdjustGunForRobotTurn(true);
	    setAdjustRadarForGunTurn(true);
		while(waves.size()>3500)	
			waves.remove(0);   //max size 3500
		do{
			turnRadarRightRadians(5);	
		}
		while(true);
	} 

  
    public void onBulletHit(BulletHitEvent e){   					//事件监听，当子弹打中敌人时能够返回敌人的能量
		enemyEnergy=e.getEnergy();
	}
    public void onHitByBullet(HitByBulletEvent e){
		enemyEnergy+=e.getPower()*3d;
	}
    public void onHitRobot(HitRobotEvent e){
		enemyEnergy-=0.6d;
	}
	public double distanceToWall(Point2D.Double p){
		return Math.min(Math.min(p.x,getBattleFieldWidth()-p.x),Math.min(p.y,getBattleFieldHeight()-p.y));
	}
	public static Point2D.Double nextPoint(Point2D.Double originPoint,double angle,double distance){
		return new Point2D.Double(originPoint.x+Math.sin(angle)*distance,originPoint.y+Math.cos(angle)*distance);
	}	
    
   	public void onScannedRobot( ScannedRobotEvent e ) {    //用雷达扫描机器人，当扫到时，雷达一直停留在敌人的身上
	    int i;
        long time=getTime();
		double moveAngle,L,disAngle,moveDistance,moveDirection,edistance,absBearing;
		Point2D.Double enemyPos,myPos,centerPos,nextP=null;
		enemyPos=nextPoint(myPos=new Point2D.Double(getX(),getY()),
			absBearing=robocode.util.Utils.normalRelativeAngle(e.getBearingRadians()+getHeadingRadians()),
            moveDistance=edistance=e.getDistance());
        centerPos=new Point2D.Double(getBattleFieldWidth()/2,getBattleFieldHeight()/2);
		
		setTurnRadarRightRadians(Math.sin(absBearing - getRadarHeadingRadians()));

		//跟着敌人的运动而运动，记录数据
		double energyChange=enemyEnergy-e.getEnergy();
        enemyEnergy=e.getEnergy();
		if(Math.abs(energyChange-1.55)<1.46){
			lastEnemyBulletPower=energyChange;
			enemyLastFireTime=time;
		}
		boolean isRam=time-enemyLastFireTime>50;

		if(Math.abs(getDistanceRemaining())<53d || isRam){
            if(Math.abs(energyChange-1.55)<1.46  || edistance<200d){
				double xieAngle=Math.abs((absBearing+Math.PI*2)%(Math.PI/2d));
				boolean isCloseToCenter=myPos.distance(centerPos)<centerPos.distance(0d,0d)/2d;
				boolean isVerticle=xieAngle<Math.PI/8d || xieAngle>Math.PI*7d/8d || isCloseToCenter;
				disAngle=2d*Math.asin(4d/(20d-lastEnemyBulletPower*3d));
				if(isVerticle){
			    	disAngle=Math.asin(8d/(20d-lastEnemyBulletPower*3d));
				}
				disAngle+=25d/edistance;
				moveDistance=Math.max(170d,moveDistance);
				do{
		    		moveAngle=Math.random()*disAngle*2-disAngle;
					L=moveDistance;
		    		if(isVerticle){
    			    	L/=Math.cos(moveAngle);
		    		}
					moveDistance-=10d;
				}while(distanceToWall(nextP=nextPoint(enemyPos,absBearing+moveAngle,-L))<24d);
			}else if(getDistanceRemaining()==0d){
				//一直绕着敌方走
		        nextP=nextPoint(enemyPos,absBearing+0.000001d,-edistance);
	    	}
			if(isRam)
			   	nextP=enemyPos;
			if(nextP!=null){
					
				//确定一个方向，找到一个最近的目的地
	    		setAhead(
	    		 (
		    		( 
		    		moveAngle = robocode.util.Utils.normalRelativeAngle(
				                  Wave.getAngle( nextP,myPos) - getHeadingRadians() 
				                  ) 
		    		) == ( moveDistance = Math.atan( Math.tan( moveAngle ) ) 
					) ? 1 : - 1 ) * myPos.distance(nextP) );	//向这目标运动
		    setTurnRightRadians( moveDistance );
			}
		}
    	setMaxVelocity( Math.abs(getTurnRemaining()) > 45 ? 0d : 8d );

		//将的到的数据
        double firedPower,catchPower;
        catchPower=Math.min(3,enemyEnergy/5d);
		firedPower=0d;
		int wavesSize=waves.size();
		
		for(i=wavesSize-1;i>=0 && i>wavesSize-201;i--)
			((Wave)waves.elementAt(i)).test(enemyPos,time);
		if((power>0 && getEnergy()-power>1.0d && Math.abs(preDiffAngle)<0.35d || edistance<150d)
			&& (getEnergy()<enemyEnergy || enemyEnergy>2d || !isRam)){
		    if(setFireBullet(power)!=null){
			    firedPower=power;
			}
		}
        power=0;
        waves.add(new Wave(myPos,firedPower,time,absBearing,edistance,(firedPower=e.getVelocity())*Math.sin(e.getHeadingRadians()-absBearing),enemyPos.distance(centerPos)));
		preDiffAngle=0d;
		if(getGunHeat()<0.4d){
   //寻找相似的样本，找到一个可以符合要求的WAVE
	    	double matchValue=Double.POSITIVE_INFINITY;//very large
    		for(i=74;i<wavesSize;i++){
	    		double comVal,comValD;
    			Wave wave=(Wave)waves.elementAt(i);
				Wave tw=(Wave)waves.elementAt(wavesSize);
	    		if(wave.willHitDiffAngle<10D && Math.abs(wave.distance-tw.distance)<2d){//is a succeded wave
				    int j=0; 
					double div=0;
				    double diffAngle=wave.willHitDiffAngle;
		    	    comVal=comValD=Math.pow((power-wave.power)/9d,2);

					do{
						comVal+=Wave.getComVal(waves.elementAt(wavesSize-j),waves.elementAt(i-j),1d)/(div=div*2+1);
						comValD+=Wave.getComVal(waves.elementAt(wavesSize-j),waves.elementAt(i-j),-1d)/(div);
					}while((j+=6)<73);

					if(comValD<comVal){
                		comVal=comValD;
						diffAngle=-wave.willHitDiffAngle;
					}
					if(comVal<matchValue){
						//absBearing+diffAngle 就是敌人的下一个角度
						
                        enemyPos=nextPoint(myPos,absBearing+diffAngle,wave.velocity);
						if(distanceToWall(enemyPos)>=18){  //判断其与墙的距离
			            	matchValue=comVal;
			        		preDiffAngle=diffAngle;
							power=catchPower;
						}
					}
	    		}
    		}
		}
        //turn gun
        setTurnGunLeftRadians(preDiffAngle=robocode.util.Utils.normalRelativeAngle(getGunHeadingRadians()-absBearing-preDiffAngle));
		scan();
    }

}
